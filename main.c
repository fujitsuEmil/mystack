#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main()
{
    char ch;
    int choice, value, loop = 1;
    TopValue  top_value;
    Stack *stack= stack_init();

    // fujitsu: co sie stanie jak stack_init zwróci NULL

    while(loop){
        printf("\n***** MENU *****\n");
        printf(" 1. Push\n 2. Pop\n 3. Display\n 4. Exit\n");
        printf("\nEnter your choice: ");

        choice = 0;

        if(!(scanf("%d", &choice))){
            fprintf(stderr, "Choice should be integer number\n");
            // fujitsu: wytłumacz co dokładnie robi poniższa pętla loop i czy ona jest naprawdę potrzebna
            while(getchar() != '\n')
                continue;
        }
        else{
            switch(choice){
            case 1:
                printf("Enter the value to be insert: ");
                scanf("%d",&value);
                push(stack, value);
                break;
            case 2:
                top_value = top(stack);
                if(!top_value.is_empty)
                    printf("Value taken from the top of the stack: %d\n", top_value.value);
                pop(stack);
                printf("stack capacity: %d\n", stack->capacity);
                break;
            case 3:
                display(stack);
                break;
            case 4:
                printf("Do you want to finish? y = yes, n = no\n");
                // fujitsu: dlaczego tu najpierw używasz getchar a później scanf. Np przy wyborze menu używasz tylko scanf.
                getchar();
                scanf("%c", &ch);
                if(ch == 'y'){
                    stack_exit(&stack);
                    loop = 0;
                }
                break;
            default:
                printf("\nWrong selection!!! Try again!!!");
                break;
            }
            // fujitsu: wytłumacz co dokładnie robi poniższa pętla loop i czy ona jest naprawdę potrzebna
            while(getchar() != '\n')
                continue;
        }
    }
    return 0;
}

