#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

/* The function creates a stack structure object and sets its initial content */
Stack *stack_init(void){
    Stack *sp = (Stack*)malloc(sizeof(Stack));
    if(sp == NULL){
        // fujitsu: wytłumacz dlaczego używasz tu fprintf. I generalnie po co tej funkcji się używa.
        //          Co to jest stderr.
        fprintf(stderr, "Memory not allocated.\n");
        exit(1);
    }
    sp->array = (int*) malloc(sizeof(int) * INIT_STACK_CAPACITY);
    if(sp->array == NULL){
        fprintf(stderr, "Memory not allocated.\n");
        exit(1);
    }
    sp->top = STACK_EMPTY;
    sp->capacity = INIT_STACK_CAPACITY;
    sp->init_capacity = INIT_STACK_CAPACITY;
    return sp;
}

/* Function pushes the item on to the stack and if necessary increases its capacity */
void push(Stack *sp, int value){
    if(sp == NULL){
        fprintf(stderr, "Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    if(sp->top + 1  == sp->capacity){
        int *temp = (int*) realloc(sp->array, sizeof(int) * sp->capacity * CAPACITY_INCREASE_FACTOR );
        if(temp != NULL){
            sp->array = temp;
            sp->capacity *= CAPACITY_INCREASE_FACTOR;
        }
        else{
            // fujitsu: jeśli realloc się nie powiedzie czy trzeba zwalniać jakąś pamięć?
            printf("Memory not allocated.\n");
            // fujitsu: a co z pamięcia wskazywaną przes wskaźnik sp, czy trzeba ja zwalniać tu?
            exit(1);
        }
    }
    sp->array[++sp->top] = value;
}

/* Function decrements the top index of the stack and reduces its capacity if needed */
void pop(Stack *sp){
    if(sp == NULL){
        fprintf(stderr, "Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    if(sp->top == STACK_EMPTY){
        fprintf(stderr, "Stack underflow\n");
    }
    else{
        // fujitsu: kiedy dokładnie ten if powinien się wykonywać? Nie do końca go rozumiemy
        if(sp->top + 1  == sp->capacity / CAPACITY_DENOMINATOR && sp->top + 1 >= sp->init_capacity){
            int *temp = (int*) realloc(sp->array, sizeof(int) * (sp->capacity / CAPACITY_DECREASE_FACTOR));
            if(temp != NULL){
                sp->array = temp;
                sp->capacity /= CAPACITY_DECREASE_FACTOR;
            }
            else{
                fprintf(stderr, "Memory not allocated.\n");
                exit(1);
            }
        }
        sp->top--;
    }
}

/* Function returns the item from the top of the stack and sets the flag is_empty if the stack is empty */
TopValue top(const Stack *sp){
    TopValue r;
    r.value = 0;
    r.is_empty = 0;
    if(sp == NULL){
        fprintf(stderr, "Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    if(sp->top == STACK_EMPTY){
        r.is_empty = 1;
        return r;
    }
    else{
        r.value = sp->array[sp->top];
        return r;
    }
}

/* Function displays all items on the stack */
void display(const Stack *sp){
    if(sp == NULL){
        fprintf(stderr, "Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    if(sp->top == -1){
        printf("Stack is empty\n");
    }
    else{
        int i;
        for(i=sp->top; i>=0; i--)
            printf("%d\n",sp->array[i]);
    }
}
/* Function releases the allocated memory for the stack */
void stack_exit(Stack **sp){
    // fujitsu: dlaczego do tej funkcji przekazuje **sp, a nie mozna *sp?;
    if(sp == NULL || *sp == NULL){
        printf("Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    free((*sp)->array);
    free((*sp));
    (*sp) = NULL;
}


